package MyList;

public class Main {
    public static void main(String[] args) {
        MyArrayList<String> list = new MyArrayList<String>();
//        void add(T elem);
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        list.add("I");
        list.add("C");
        list.add("D");
        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%s; ", list.get(i));
        }
        System.out.println("\n");

//        void insert(T elem, int index);
        list.insert("F", 3);
        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%s; ", list.get(i));
        }
        System.out.println("\n");

//        void remove(int index)
        list.remove(3);
        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%s; ", list.get(i));
        }
        System.out.println("\n");

//        void remove(T value);
        list.remove("C");
        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%s; ", list.get(i));
        }
        System.out.println("\n");

//        int indexOf(T value);
        System.out.println(list.indexOf("S") + "\n");

//        boolean contains(T value);
        System.out.println(list.contains("D") + "\n");

//        boolean containsAll(MyList<T> values);
        MyArrayList<String> list2 = new MyArrayList<String>();
        list2.add("A");
        list2.add("B");
        list2.add("I");
        list2.add("D");

        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%s; ", list.get(i));
        }
        System.out.println("\n");
        for (int i = 0; i < list2.size(); i++) {
            System.out.printf("%s; ", list2.get(i));
        }
        System.out.println("\n");
        System.out.println(list.containsAll(list2));

    }
}
