package MyList;public interface MyList<T> {
    void add(T elem); // добавление элемента
    void insert(T elem, int index); // добавление перед указанным индексом
    void remove(int index); //удаление по индексу
    void remove(T value); //удаление по значению (удаляет первое совпадение)
    int indexOf(T value); //ищет индекс первого совпадения, если нету то -1
    boolean contains(T value); //возвращает true если массив содержит элемент
    boolean containsAll(MyList<T> values); //возвращает, если содержатся все значения из values
    T get(int index);
    void set(int index, T value);
    int size();

}
