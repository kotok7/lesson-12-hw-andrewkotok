package MyList;

import java.util.Arrays;


public class MyArrayList<T> implements MyList<T> {
    private static final int INITIAL_SIZE = 8;
    private static final int SCALE_FACTOR = 2;
    private int size = 0;
    private Object[] arr = new Object[INITIAL_SIZE];


    @Override
    public void add(T elem) {
        if (arr.length == size) expand();
        arr[size++] = elem;
    }

    @Override
    public void insert(T elem, int index) {
        validateIndex(index);
        Object[] tmp = new Object[size + 1];
        for (int i = 0; i < tmp.length; i++) {
            if (i < index) {
                tmp[i] = arr[i];
            } else if (i == index) {
                tmp[i] = elem;
                size++;
            } else {
                tmp[i] = arr[i - 1];
            }
        }
        arr = Arrays.copyOf(tmp, size);
    }

    @Override
    public void remove(int index) {
        validateIndex(index);
        Object[] tmp = new Object[size - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i == index) {
                size--;
                continue;
            }
            tmp[j++] = arr[i];
        }
        arr = Arrays.copyOf(tmp, size);
    }

    @Override
    public T get(int index) {
        validateIndex(index);
        return (T) arr[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void set(int index, T value) {
        validateIndex(index);
        arr[index] = value;
    }

    @Override
    public boolean containsAll(MyList<T> values) {
        int count = 0;
        if (values.size() > arr.length) return false;
        if (values.size() == 0 && arr.length == 0) return false;
        for (Object o : arr) {
            if (values.get(count) == o) {
                count++;
            }
            if (count == values.size()) return true;
        }
        return false;
    }

    @Override
    public boolean contains(T value) {
        for (Object o : arr) {
            if (o == value) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int indexOf(T value) {
        int k = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                k = i;
                break;
            }
        }
        return k;
    }

    @Override
    public void remove(T value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                remove(i);
                break;
            }
        }
    }

    private void expand() {
        int newLength = arr.length * SCALE_FACTOR;
        arr = Arrays.copyOf(arr, newLength);
    }

    private void validateIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }
    }
}
